export default {
  namespaced: true,
  state() {
    return {
      requests: []
    };
  },
  mutations: {
    addRequest(state, payload) {
      state.requests.push(payload);
    },
    setRequests(state, payload) {
      state.requests = payload;
    }
  },
  actions: {
    async contactCoach(context, payload) {
      const newRequest = {
        userEmail: payload.email,
        message: payload.message
      };
      const response = await fetch(
        `https://vue-coach-app-dc88b-default-rtdb.firebaseio.com/requests/${payload.coachId}.json`,
        { method: 'POST', body: JSON.stringify(newRequest) }
      );

      const responseData = await response.json();
      newRequest.coachId = payload.coachId;
      newRequest.id = responseData.name; //responseData.name is the ID that firebase adds to
      // the request we are sending, and with the statement above, we are adding that id to the object we created for the Vuex store.
      if (!response.ok) {
        const error = new Error(response.message || 'Failed to send request');
        throw error;
      }
      context.commit('addRequest', newRequest);
    },
    async fetchRequests(context) {
      const token = context.rootGetters.token;
      const coachId = context.rootGetters.userId;
      const response = await fetch(
        `https://vue-coach-app-dc88b-default-rtdb.firebaseio.com/requests/${coachId}.json?auth=${token}`
      );
      const responseData = await response.json();

      if (!response.ok) {
        const error = new Error(response.message || 'Failed to fetch request');
        throw error;
      }
      const requests = [];
      for (const key in responseData) {
        const newReq = {
          coachId: coachId,
          id: key,
          message: responseData[key].message,
          userEmail: responseData[key].userEmail
        };
        requests.push(newReq);
      }
      context.commit('setRequests', requests);
    }
  },
  getters: {
    getRequests(state, _, _2, rootGetters) {
      const coachId = rootGetters.userId;
      return state.requests.filter(req => req.coachId === coachId);
    },
    hasRequests(state, getters) {
      return getters.getRequests && getters.getRequests.length > 0;
    }
  }
};
