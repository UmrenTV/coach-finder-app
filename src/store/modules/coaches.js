export default {
  namespaced: true,
  state() {
    return {
      lastFetch: null,
      coaches: []
    };
  },
  getters: {
    coaches(state) {
      return state.coaches;
    },
    hasCoaches(state) {
      /* console.log(state.coaches.length); */
      return state.coaches && state.coaches.length > 0;
    },
    isCoach(_, getters, _2, rootGetters) {
      const coaches = getters.coaches;
      const userId = rootGetters.userId;
      return coaches.some(coach => coach.id === userId); // some is a built-in method which returns true if some coach fills the criteria
    },
    shouldUpdate(state) {
      const lastFetch = state.lastFetch;
      if (!lastFetch) {
        return true;
      }
      const currentTimeStamp = new Date().getTime();
      return (currentTimeStamp - lastFetch) / 1000 > 60;
      // This checks if the last fetch has been more than a minute ago.
    }
  },
  mutations: {
    registerCoach(state, payload) {
      state.coaches.push(payload);
    },
    setCoaches(state, payload) {
      state.coaches = payload;
    },
    setFetchTimeStamp(state) {
      state.lastFetch = new Date().getTime();
    }
  },
  actions: {
    // Instead of using then at every promise we'll use async await syntax here. It's the same, it's just fancier
    async registerCoach(context, data) {
      const userId = context.rootGetters.userId;
      const coachData = {
        /* id: context.rootGetters.userId, We'll get rid of this, because we need the object built, if we gonna send it through an http request. We cannot send rootGetters in Firezilla, it has to be id, so we'll first save it in a variable.*/
        firstName: data.first,
        lastName: data.last,
        description: data.desc,
        hourlyRate: data.rate,
        areas: data.areas
      };
      const token = context.rootGetters.token;
      // We do this this way, because as you can see we have avoided sending the id in the object, but rather we create separate path for each coach by manipulating the path we fetching from, utilizing code injection by using `` instead of '' (backtick vs quotes)
      const response = await fetch(
        `https://vue-coach-app-dc88b-default-rtdb.firebaseio.com/coaches/${userId}.json?auth=${token}`,
        {
          method: 'PUT',
          body: JSON.stringify(coachData)
        }
      );

      const responseData = await response.json();

      if (!response.ok) {
        const error = new Error(responseData.message || 'Failed to fetch!');
        throw error;
      }
      context.commit('registerCoach', {
        ...coachData,
        id: userId
      });
    },
    async loadCoaches(context, payload) {
      if (!context.getters.shouldUpdate && !payload.forceRefresh) {
        return; //don't execute the below code if shouldUpdate is not true.
      }
      const response = await fetch(
        `https://vue-coach-app-dc88b-default-rtdb.firebaseio.com/coaches.json`
      );
      const responseData = await response.json();

      if (!response.ok) {
        // error handling ..
      }
      const coaches = [];

      for (const key in responseData) {
        const coach = {
          id: key,
          firstName: responseData[key].firstName,
          lastName: responseData[key].lastName,
          description: responseData[key].description,
          hourlyRate: responseData[key].hourlyRate,
          areas: responseData[key].areas
        };
        coaches.push(coach);
      }
      context.commit('setCoaches', coaches);
      context.commit('setFetchTimeStamp');
    }
  }
};
