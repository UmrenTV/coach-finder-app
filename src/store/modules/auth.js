let timer; // we set this GLOBAL variable here, to check whether the timer for token
// expiration already exists, and if it is then set the time for the time left, to avoid duplicate timers for the same purpose.

export default {
  state() {
    return {
      userId: null,
      token: null,
      //tokenExpiration: null, - not using it anymore coz of localStorage.
      didAutoLogout: false
    };
  },
  getters: {
    userId(state) {
      return state.userId;
    },
    token(state) {
      return state.token;
    },
    isAuthenticated(state) {
      return !!state.token;
    },
    didAutoLogout(state) {
      return state.didAutoLogout;
    }
  },
  mutations: {
    setUser(state, payload) {
      state.token = payload.token;
      state.userId = payload.userId;
      state.didAutoLogout = false;
      // state.tokenExpiration = payload.tokenExpiration; Same as below, we removing it from here too.
      // we don't even need it in Vuex. Since we have set in the actions below to persist in localStorage already.
    },
    setAutoLogout(state) {
      state.didAutoLogout = true;
    }
  },

  actions: {
    logout(context) {
      localStorage.removeItem('token');
      localStorage.removeItem('userId');
      localStorage.removeItem('expiresIn');

      clearTimeout(timer); //We are clearing any Timeouts we might have running from the login action

      context.commit('setUser', {
        userId: null,
        token: null
        // tokenExpiration: null this not needed anymore.
      });
    },
    // we could have also made 2 actions (login/logout) and spread/copy the payload
    // with the ...payload, action: 'login' which will spread the original payload,
    // in the new object we are sending + add the action key, with 'login' value, instead
    // of manually retyping the payload into: email: payload.email, password: payload.password, etc
    async authAction(context, payload) {
      let url;
      if (payload.action === 'login') {
        url =
          'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAQMq1JGKeGrfu6i9Mia1fmcRQ8z_4f-CI';
      } else if (payload.action === 'signup') {
        url =
          'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyAQMq1JGKeGrfu6i9Mia1fmcRQ8z_4f-CI';
      }
      const response = await fetch(url, {
        method: 'POST',
        body: JSON.stringify({
          email: payload.email,
          password: payload.password,
          returnSecureToken: true
        })
      });
      const responseData = await response.json();

      if (!response.ok) {
        const error =
          new Error(responseData.error.message) || 'Failed to create user.';
        throw error;
      }
      // + is added to responseData.expiresIn to make sure it's converted to number.
      //const expiresIn = 5000; //convert to miliseconds
      const expiresIn = +responseData.expiresIn * 1000; //convert to miliseconds
      const expirationDate = new Date().getTime() + expiresIn; //get the current time + add the time in miliseconds of the ExpiresIn token, so we know when in future we should auto-logout
      localStorage.setItem('token', responseData.idToken);
      localStorage.setItem('userId', responseData.localId);
      localStorage.setItem('expiresIn', expirationDate);
      // this is how we save to local storage (browser) so the session data can survive reloads

      timer = setTimeout(function() {
        context.dispatch('autoLogout');
      }, expiresIn);

      context.commit('setUser', {
        token: responseData.idToken,
        userId: responseData.localId
        // tokenExpiration: expirationDate we are removing this because we don't even need it in Vuex.
        // we already have it in local storage, and this matters only when we reload the page which we check in app.vue in created() life hook
      });
    },
    tryLogin(context) {
      const token = localStorage.getItem('token');
      const userId = localStorage.getItem('userId');
      const tokenExpiration = localStorage.getItem('expiresIn');

      const expiresIn = +tokenExpiration - new Date().getTime();
      // if the expiresIn is less than 10 seconds, or it's negative number, no point of logging the user in.
      if (expiresIn < 0) {
        return;
      }

      timer = setTimeout(function() {
        context.dispatch('autoLogout');
      }, expiresIn);

      if (token && userId) {
        context.commit('setUser', {
          token: token,
          userId: userId
          //tokenExpiration: null Also here we don't need it anymore.
        });
      }
    },
    autoLogout(context) {
      context.dispatch('logout');
      context.commit('setAutoLogout');
    }
  }
};
