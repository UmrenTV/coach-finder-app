import { createStore } from 'vuex';
import coachesModule from './modules/coaches.js';
import requestsModule from './modules/requests.js';
import authModule from './modules/auth.js';

const store = createStore({
  modules: {
    coaches: coachesModule,
    requests: requestsModule,
    auth: authModule
  },
  state() {
    return {
      hostedUrl: 'http://find-coach-app.s3-website.eu-central-1.amazonaws.com/',
      dbUrl: 'https://vue-coach-app-dc88b-default-rtdb.firebaseio.com'
    };
  },
  getters: {
    hostedUrl(state) {
      return state.hostedUrl;
    }
  }
});
export default store;
