import store from './store/index.js';
import { createRouter, createWebHistory } from 'vue-router';
// import { defineAsyncComponent } from 'vue';
/* NOTE: I leave intentionally one definition of defineAsyncComponent way in case I need to 
remind myself of the definition and syntax. However, according to the Vue3 Documentation, we should
be using dynamic imports for routes, with the const X = () => import('xPath') instead, because it litteraly
does the same thing, just better. It loads the components used in routes dynamically whenever they are accessed.
Here is a link to read more on that: https://next.router.vuejs.org/guide/advanced/lazy-loading.html
That being said, below I have replaced all defineAsyncComponent stuffs with the above syntax.
NOTE2: for loading Components async we use defineAsyncComponent still, this is just for routes.*/

//import CoachDetail from './pages/coaches/CoachDetail.vue';
// import CoachList from './pages/coaches/CoachList.vue';
// import CoachRegistration from './pages/coaches/CoachRegistration.vue';
// import ContactCoach from './pages/requests/ContactCoach.vue';
//import RegistrationsReceived from './pages/requests/RequestsReceived.vue';
// import NotFound from './pages/NotFound.vue';
// import UserAuth from './pages/auth/UserAuth.vue';

// this way, with defineAsyncComponent we can lazy-load components, routes, anything really.
// The local components as well inside the components. It is very efficient way to load stuffs on demand.

/* const CoachList = () => import('./pages/coaches/CoachList.vue');
const CoachDetail = () => import('./pages/coaches/CoachDetail.vue');
const CoachRegistration = () => import('./pages/coaches/CoachRegistration.vue');
const RegistrationsReceived = () =>
  import('./pages/requests/RequestsReceived.vue');
const ContactCoach = () => import('./pages/requests/ContactCoach.vue');
const UserAuth = () => import('./pages/auth/UserAuth.vue'); */

// This is intentionally left like this. Read more on the text at top.
// const NotFound = defineAsyncComponent(() => import('./pages/NotFound.vue'));

// So I have commented out the second way as well, because according to some updated syntax rules
// it's EVEN BETTER to dynamically import the components directly inside the routes like this:
// component: () = import('/path'), so that is what we'll do below.

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', redirect: '/coaches' },
    {
      path: '/coaches',
      component: () => import('./pages/coaches/CoachList.vue')
    },
    {
      path: '/coaches/:id',
      component: () => import('./pages/coaches/CoachDetail.vue'),
      props: true, // Since this component is loaded through router, the regular props won't work. So we have to enable the router props, which means the dinamic value that is passed in the route, in this case :id.
      children: [
        {
          path: 'contact',
          component: () => import('./pages/requests/ContactCoach.vue')
        } // /coaches/c1/contact
      ]
    },
    {
      path: '/register',
      component: () => import('./pages/coaches/CoachRegistration.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/requests',
      component: () => import('./pages/requests/RequestsReceived.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/auth',
      component: () => import('./pages/auth/UserAuth.vue'),
      meta: { requiresUnauth: true }
    },
    { path: '/:notFound(.*)', component: () => import('./pages/NotFound.vue') }
  ]
});

// SO this is the BEST practice when it comes to loading the routes. Into the router configuration
// dynamically loading them directly in the component: key and using the import syntax as value.

router.beforeEach(function(to, _, next) {
  // _ should be from. But we are not using it here.
  if (to.meta.requiresAuth && !store.getters.isAuthenticated) {
    next('/auth');
  } else if (to.meta.requiresUnauth && store.getters.isAuthenticated) {
    next('/coaches');
  } else {
    next();
  }
});

export default router;
